## Course URL   
* https://courses.cs.ut.ee/2017/DM/fall/Main/Homeworks

## Programming Language used for Data analysis and Machine Learning     
1. R/ R Studio   
2. SQL    

## Coursework Topics & Homeworks       
1. Introduction to data mining    
2. First look at the data    
3. Exploration of data    
4. Basic statistics   
5. Frequent pattern mining   
6. Data discovery with Tableau software   
7. Machine learning 1: Introduction    
8. Machine learning 2: Classification   
9. Machine learning 3: ROC analysis and regression   
10. Machine learning 4: Deep learning    
11.  Clustering and dimensionality reduction    
12. Databases and big data    
13. Applications: Graph mining and natural language processing    
14. Uncertainty in data mining    

* Kaggle competition: https://www.kaggle.com/c/machine-learning-competition-ut     

