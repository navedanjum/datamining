---
title: "Homework5"
author: "Navedanjum Ansari"
date: "October 26, 2017"
output:
  pdf_document: default
  word_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Excercise 1
```{r}
setwd("C:/Subjects/sem1/DataMining/submission")
knndf <- read.csv("knn_data.csv", header = TRUE)

#levels(knndf$class) # currently there are no levels
knndf$class <- factor(knndf$class) # Converting the class column to factors

str(knndf) # Check whether the data is structured or not
table(knndf$class) #This helps to get the number of each class i.e class 0 and class 1

```
### Visualization
```{r KNN visuals, echo=FALSE}
library(ggplot2)
p <- ggplot(knndf, aes(x_coord, y_coord, label = rownames(knndf))) + geom_text(aes(colour = factor(class)))
p <- p + scale_x_continuous(minor_breaks = seq(0 , 10, 1), breaks = seq(0, 10, 1)) + 
     scale_y_continuous(minor_breaks = seq(0 , 10, 1), breaks = seq(0, 10, 1))
p + coord_fixed(ratio = 1)
```

### Explaination:
**Ignoring calculation of Eucledian distance as distance is clear from the plot**    

**Observations:**

**For coordinate instance  (3,5):**


                  * when k=1  nearest neighbour is (2,4)  
                    therefor class = 1

                  * when k=5 nearest neighbours are (2,4), (3,3), (3,7), (1,6) and (5,6)  
                    therefor class = 1 as 3 out of 5 neighbours have class as 1

                  * when k=10 nearest neighbour consist of whole dataset which consist of  
                    60% of datasets have class as 0  
                    therefor  when k=10  class = 0 for instance (3,5)       

We do not get the same prediction for k=10 because when K grows larger in this case the total size of  
the dataset the prediction is same as the value of class for the majority of dataset values i.e class 0  


## Excercise 2
a) *Answer*      
Dataset is about images of digits taken from handrwritten scanned documents which are normalized in size
and centered.Each image is 28 x 28 pixel square image.

Each dataset is either of the 10 classes which are
digits from 0 to 9.   

28 x 28 pixel is flatten into a a vector of 784-dimensionality

```{r}
mnist <- readRDS("MNIST.RDS")
levels(mnist$y)
mnist$y <- factor(mnist$y)
str(mnist)
```
Number of digits from each of the 10 classes i.e 0,1,2,3,4,5,6,7,8,9 is as follows:

```{r}
table(mnist$y)
```

b) *Answer*       
In majority of examples that I tried i.e (18 examples) for different classes, the image and corresponding label is correct.     
However in one case the label is ambiguous because of the image is distorted. By reading the image, I believe the image is more close to digit 6 but its corresponding label is digit as 5 

#Ambiguous label case:
```{r}
# first we need to define colors:
colors <- c('black', 'white')
cus_col <- colorRampPalette(colors=colors)
# Ambiguous label digit could be 5 or 6
index <- 30567
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
print(paste("Correct label of the first image is:", mnist$y[30567]))
```


```{r}
index <- 20464
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
print(paste("Correct label of the first image is:", mnist$y[20464]))

```

```{r}

#Digit 1
index <- 31912
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
print(paste("Correct label of the first image is:", mnist$y[31912]))

index <- 4
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
print(paste("Correct label of the first image is:", mnist$y[4]))

```

```{r}
#Digit 2
index <- 6
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
#The code for getting the label of the image:
head(mnist$y)
print(paste("Correct label of the first image is:", mnist$y[6]))


index <- 26
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
print(paste("Correct label of the first image is:", mnist$y[26]))

index <- 10989
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
print(paste("Correct label of the first image is:", mnist$y[10989]))
```

```{r}
#Digit 3
index <- 44440
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
print(paste("Correct label of the first image is:", mnist$y[44440]))
```

```{r}
#Digit 4
index <- 989
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
print(paste("Correct label of the first image is:", mnist$y[989]))

index <- 4440
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
print(paste("Correct label of the first image is:", mnist$y[4440]))
```

```{r}
#Digit 5
index <- 59998
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
print(paste("Correct label of the first image is:", mnist$y[59998]))
```
```{r}
#Digit 6
index <- 59999
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
print(paste("Correct label of the first image is:", mnist$y[59999]))
```

```{r}
#Digit 7
index <- 30505
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
print(paste("Correct label of the first image is:", mnist$y[30505]))

```
```{r}
#Digit 8
index <- 50989
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
print(paste("Correct label of the first image is:", mnist$y[50989]))

index <- 51900
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
print(paste("Correct label of the first image is:", mnist$y[51900]))

```
```{r}
index <- 5
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
#The code for getting the label of the image:
head(mnist$y)
print(paste("Correct label of the first image is:", mnist$y[5]))

#Digit 9
index <- 20
img <- array(mnist$x[index,],dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))
print(paste("Correct label of the first image is:", mnist$y[20]))

```

c) Visualize the labels usings ggplot2 library and geom_bar to visualize class distribution      

```{r digits }
X <- data.frame('digits' = mnist$y)
library(ggplot2)
g <- ggplot(data=X, aes(x=digits, fill=digits)) + geom_bar(colour="black", stat="count")
g

```

d) Answer      
```{r, echo=FALSE}
pixel <- data.frame('value' = mnist$x[,400])
 
ggplot(pixel, aes(x = value)) +
  geom_histogram(binwidth = 0.1) +
  theme_bw()
```



     
       

Visualization of 400th attribute for different digits using facet_wrap.     
   i.e intensity vlaue (0 to 1) of 400th pixel for all the classes.    
   
```{r pixel value distribution}
pixel <- data.frame('value' = mnist$x[,400] , digits= mnist$y)
fw <- ggplot(pixel, aes(x = value)) +   geom_histogram(binwidth = 0.1) + theme_bw()
fw + facet_wrap(~digits)
```


## Excercise 3      
a. Split the data into training and testing data by filling in the ? parts in the following code:   

```{r }
# You can use set.seed() in order to reproduce stochastic results
set.seed(1111)
new_indx <- sample(c(1:nrow(mnist$x)), size = 4000, replace = FALSE)
 
sample_img <- mnist$x[new_indx, ]
sample_labels <- mnist$y[new_indx]
 
train_img <- sample_img[1:3000,]
train_labels <- sample_labels[1:3000]
 
test_img <- sample_img[3001:4000,]
test_labels <- sample_labels[3001:4000]
 
str(train_img) # Make sure you have 3000 rows here
str(test_img) # Make sure you have 1000 rows here

```

b) function would calculate and return the euclidean distance between img1 and img2
```{r }
dist <- function(img1, img2) {
  result <- 0
  for(i in 1:768){
    result <- result + (img1[i]-img2[i])^2
  }
  result <- sqrt(result)
  return(result)
}

print(paste("Distance between images of class", train_labels[2], "and", train_labels[8], "is", dist(train_img[2,], train_img[8,])))
print(paste("Distance between images of class",train_labels[2], "and", train_labels[4], "is", dist(train_img[2,], train_img[4,])))
```

c) 

```{r}
unknown_img <- test_img[1,]
true_label <- test_labels[1]

#c.1. Compute all distances from the `unknown_img` to the images in the dataset
all_distances <- apply(train_img, 1, function(img) dist(unknown_img, img))
head(all_distances)

#c.2. Now let's find out which image is closest to our `unknown_img`
closest_index <- which.min(all_distances)

#c.3. Almost done, now report a label with index i in labels by filling in the code.
predicted_label <- train_labels[closest_index]

(predicted_label == true_label)
print(paste("Predicted class for the first image is", predicted_label ,"and the true label is",  true_label))

#Use code from the previous exercise to plot the first example and visually confirm it's label.

img <- array(unknown_img,dim=c(28,28))
img <- img[,28:1]
image(img, col=cus_col(256))

```


d)

```{r}
classify <- function(unknown_img) {
    all_distances <- apply(train_img, 1, function(img) dist(unknown_img, img))
    closest_index <- which.min(all_distances)
    predicted_label <- train_labels[closest_index]
    return(predicted_label)
}

print(paste("Predicted class for the first image is", classify(unknown_img),"and the true label is",  true_label))
```


e)

```{r}
classify_knn <- function(unknown_img, k = 5) {
  # This step we already know from the previous exercises
  all_distances <- apply(train_img, 1, function(img) dist(unknown_img, img))
 
  # We need to get indexes of K smallest distances
  # (hint: use functions order() and head())
  knn = head(order(all_distances, decreasing = FALSE), k)
 
  # you can print potential predictions
  #print(train_labels[knn])
  #print(names(sort(table(train_labels[knn]), decreasing = TRUE)))
 
  # Very small step is left, return the most frequently predicted label
  return(names(sort(table(train_labels[knn]), decreasing = TRUE))[1])
}

#Test this version of KNN, experiment with different `K`s
print(paste("Predicted class for the first image is", classify_knn(unknown_img, k = 100),"and the true label is",  true_label))

print(paste("Predicted class for the first image is", classify_knn(unknown_img, k = 50),"and the true label is",  true_label))

print(paste("Predicted class for the first image is", classify_knn(unknown_img, k = 10),"and the true label is",  true_label))

print(paste("Predicted class for the first image is", classify_knn(unknown_img, k = 3 ),"and the true label is",  true_label))

print(paste("Predicted class for the first image is", classify_knn(unknown_img, k = 250),"and the true label is",  true_label))

print(paste("Predicted class for the first image is", classify_knn(unknown_img, k = 500),"and the true label is",  true_label))

```

## Excercise 4

```{r }
#a. Classify all test images and store them into a separate variable `test_predicted`, choose `k` = 5. It might be a bit slowish

#test_predicted <- "... your code here ..."
#head(test_predicted)

test_predicted <- apply(test_img, 1, function(img) classify_knn(img, k = 5)) 
head(test_predicted)

knn_accuracy = sum((test_predicted == test_labels))/length(test_labels) 
print(paste("Final accuracy of our nearest neighbor classifier is", knn_accuracy,"- not bad!"))

```

b. Now let us use the caret package to train and test the k Nearest Neigbor classifier, avoiding the need    to implement it ourselves.

```{r }
library(caret)
 
# We will discuss this line further in more details,
# we need it now as without it, caret tries to be very smart
# and training takes too much time...
ctrl <- trainControl(method="none", number = 1)
 
# we should use train_labels as factors, otherwise caret thinks that
# this is a regression problem
(knn_fit <- train(y = as.factor(train_labels), x = data.frame(train_img), method = "knn", trControl = ctrl, tuneGrid = data.frame(k = 5)))

```
```{r }
test_predicted = predict(knn_fit, data.frame(test_img))
print(paste("Accuracy of caret nearest neighbor classifier is", sum(test_predicted == test_labels)/length(test_labels)))

```
```{r }
#A useful way to study classification results is by examining the confusion matrix, which counts pairs (true_class, predicted_class)
confusionMatrix(test_predicted, test_labels)
```
```{r }
#train 2 K-NN classifiers with different K's. Report the accuracies of both models on training and testing #data. K=10 and K=25
library(caret)
 ctrl <- trainControl(method="none", number = 1)

(knn_fit <- train(y = as.factor(train_labels), x = data.frame(train_img), method = "knn", trControl = ctrl, tuneGrid = data.frame(k = 10)))

test_predicted = predict(knn_fit, data.frame(test_img))
print(paste("Accuracy of caret nearest neighbor classifier on test data with k=10 is", sum(test_predicted == test_labels)/length(test_labels)))


train_predicted = predict(knn_fit, data.frame(train_img))
print(paste("Accuracy of caret nearest neighbor classifier on train data with k=10 is", sum(train_predicted == train_labels)/length(train_labels)))

(knn_fit <- train(y = as.factor(train_labels), x = data.frame(train_img), method = "knn", trControl = ctrl, tuneGrid = data.frame(k = 25)))

test_predicted = predict(knn_fit, data.frame(test_img))
print(paste("Accuracy of caret nearest neighbor classifier on test data with k=25 is", sum(test_predicted == test_labels)/length(test_labels)))

train_predicted = predict(knn_fit, data.frame(train_img))
print(paste("Accuracy of caret nearest neighbor classifier on train data with k=25 is", sum(train_predicted == train_labels)/length(train_labels)))


```



c. C5.0 classifier    

```{r }
library(caret)
library(plyr)
ctrl <- trainControl(method="none", number = 1)

(knn_fit <- train(y = as.factor(train_labels), x = data.frame(train_img), method = 'C5.0', trControl = ctrl, tuneGrid = data.frame(winnow = c(TRUE), trials=10, model="tree")))

test_predicted = predict(knn_fit, data.frame(test_img))
print(paste("Accuracy of caret nearest neighbor classifier on test data is", sum(test_predicted == test_labels)/length(test_labels)))

```
```{r }
#Case 1
confusionMatrix(test_predicted, test_labels)
```

```{r }
library(caret)
library(plyr)
ctrl <- trainControl(method="none", number = 1)
(knn_fit <- train(y = as.factor(train_labels), x = data.frame(train_img), method = 'C5.0', trControl = ctrl, tuneGrid = data.frame(winnow = c(TRUE), trials=5, model="tree")))
test_predicted = predict(knn_fit, data.frame(test_img))
print(paste("Accuracy of caret nearest neighbor classifier on test data is", sum(test_predicted == test_labels)/length(test_labels)))


train_predicted = predict(knn_fit, data.frame(train_img))
print(paste("Accuracy of caret nearest neighbor classifier on train data is", sum(train_predicted == train_labels)/length(train_labels)))

```
```{r }
#Case 2
confusionMatrix(test_predicted, test_labels)
```

```{r }
library(caret)
library(plyr)
ctrl <- trainControl(method="none", number = 1)
(knn_fit <- train(y = as.factor(train_labels), x = data.frame(train_img), method = 'C5.0', trControl = ctrl, tuneGrid = data.frame(winnow = c(TRUE), trials=100, model="tree")))
test_predicted = predict(knn_fit, data.frame(test_img))
print(paste("Accuracy of caret nearest neighbor classifier on test data using C5.0, when trials=100  is", sum(test_predicted == test_labels)/length(test_labels)))

train_predicted = predict(knn_fit, data.frame(train_img))
print(paste("Accuracy of caret nearest neighbor classifier on train data using C5.0, when trials=100  is", sum(train_predicted == train_labels)/length(train_labels)))



```
```{r }
#Case 3
confusionMatrix(test_predicted, test_labels)
```
d. Summarise the results by nicely showing all accuracies you calculated     
(on both training and testing data). Comment on the results. Which model worked the best?   

###Algorithm KNN   

Accuracy of caret nearest neighbor classifier on test data with k=10 is 0.898"
Accuracy of caret nearest neighbor classifier on train data with k=10 is 0.932"
Accuracy of caret nearest neighbor classifier on test data with k=25 is 0.882"
Accuracy of caret nearest neighbor classifier on train data with k=25 is 0.9036"
** Accuracy depends on optimum value of k as in this case observed to be more accurate at k=10**


###Algorithm C5.0      

Accuracy of caret nearest neighbor classifier is 0.836 using C5.0 when trials =5
Accuracy of caret nearest neighbor classifier on test data using C5.0 is 0.836 when  trials =10
Accuracy of caret nearest neighbor classifier on train data using C5.0 is 0.99 when  trials =10
Accuracy of caret nearest neighbor classifier on test data using C5.0, when trials=100  is 0.9
Accuracy of caret nearest neighbor classifier on train data using C5.0, when trials=100  is 1
** With increased trials the accuracy is increased **



