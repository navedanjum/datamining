setwd("C:/Subjects/sem1/DataMining/submission")
getwd()

# The goal in this exercise is to study the frequencies of itemsets in a dataset with transactions
# covering 8 items. Please first run the following commands to build the dataset:
  
# required only once, please comment this out after the first time
install.packages("arules")
library(arules)

data = list(
  c('B','D','F','H'),
  c('C','D','F','G'),
  c('A','D','F','G'),
  c('A','B','C','D','H'),
  c('A','C','F','G'),
  c('D','H'),
  c('A','B','E','F'),
  c('A','D','F','G','H'),
  c('A','C','D','F','G'),
  c('D','F','G','H'),
  c('A','C','D','E'),
  c('B','E','F','H'),
  c('D','F','G'),
  c('C','F','G','H'),
  c('A','C','D','F','H')
)

data = as(data, "transactions")
# Next, use the command inspect(data) to take a look at the dataset,
# but please do not include the result into the report PDF. By performing all counting
# manually (without programming), please answer the following questions:

inspect(data)
# (1a) Calculate the support and support count of patterns {D}, {D,F} and {D,F,G}
# Answer:
# {D}= 11
# {D,F} = 8
# {D,F,G} = 6
#
# Considering total of 15 transactions, we have support of atleast 9 count i.e support of 9/15 is 60%
# Support for {D} is 11/15 is 0.733
# Support for {D,F} is 8/15 is 0.533
# Support for {D,F,G} is 6/15 is 0.4


# (1b) Report the row indices (identifiers) of transactions which include the pattern {D,F,G}
# Answer:
# Indices = [2,3,8,9,10,13]

# (1c) Explain what anti-monotonicity of support means, in the example of these patterns {D}, {D,F} and {D,F,G}
# Answer: Support count is anti-monotone which means if an itemset violates a constraint then all of it supersets also
          # violates the constraints.
          # For Example, in case of itemset {D}, let's have a count constraint of 11 transaction i.e C >= 11
          # Here {D} failed to satisfy C hence all of it superset {D,F}, {D,F,G} and other will not satisfy C and have count less
          # than 11 .

          # Similarly for itemset {D,F} , let's have constraint C >= 9
          # {D,F} violates C because support count of {D,F} is 8 hence all of the supersets of {D,F} violates C
          # For {D,F,G}, let's have C >= 7 , {D,F,G} volates C hence all i's superset violates C
          # This proves support count of an itemset is anti-monotone and we can prune the itemset and it's superset

          # Also if an itemset satisfies the constraint C, then all of it's subset-itemsets satisfies C
          # For example if for itemset {D,F,G} , C is count >=6 , {D,F,G} satisfies C hence it's subset
          # {D}, {D,F}, {DG}, {F},{F,G} and {G} also satisfies C
          # This shows support count is anti-monotone and this property is known as anti-monotonicity of support.




library(dplyr)

find_freq_itemsets = function(data, min_support_count) {
  # convert support count into support
  min_support = min_support_count / length(data)
  
  # find itemsets with support>=min_support
  itemsets = eclat(data, parameter=list(support=min_support))
  
  # convert to data.frame, it is easier to manipulate
  itemsets = as(itemsets, "data.frame") 
  
  # items are factors, convert them to strings
  itemsets$items = as.character(itemsets$items)
  
  # sort by length of string, and among equal-length strings sort alphabetically
  itemsets = itemsets %>% arrange(nchar(items), items) 
}

itemsets = find_freq_itemsets(data,5)
print(itemsets)


# Please, answer the following questions:

# (1d) How many itemsets could be generated in total from 8 items?
# Answer: 256 possible itemsets including {} set. (pow(2,8))

# (1e) What percentage of these itemsets have positive support
# (occur at least once in the data)? Use find_freq_itemsets(data,1) to find it out.
items_percentage = count(find_freq_itemsets(data,1)) * 100/256
print(items_percentage)
# Answer: 37.5%

# Let us now consider the task of finding all frequent patterns of size 3 with minimum
# support count 5.
#
# (1f) Naive method would have to look through all possible subsets of size 3.
# How many subsets of size 3 out of 8 items are there altogether?
# Calculate this with function choose(8,3).
choose(8,3)

# (1g) Apriori builds candidate 3-sets out from frequent 2-sets.
# Look at the output of find_freq_itemsets(data,5) and manually (without programming)
# find and report all 3-sets that can be obtained as a union of two frequent 2-sets
# (please do not discard any of the resulting sets yet, this will be done in next steps).
print(find_freq_itemsets(data,5))
# Answer: {A,C,D} {A,D,F} {A,C,F}
#         {C,D,F}
#         {D,F,G} {D,F,H} {D,G,H}
#         {F,G,H}

# (1h) Study the 3-sets reported in (1g) and discard all the 3-sets for which some subset
#       of size 2 is not frequent. Report the remaining candidate 3-sets.
# Answer: {G,H} is not frequent therefore,discarding all 3-sets with {G,H} as a subset
#
#         {A,C,D} {A,D,F} {A,C,F}
#         {C,D,F}
#         {D,F,G} {D,F,H}

# (1i) Instead of counting the frequencies of all candidate 3-sets just report all the
# frequent 3-sets from the output of find_freq_itemsets(data,5).
# Answer: {D,F,G} with support of 40%


# Exercise 2 
# (2a) Create and report all possible association rules where the union of the antecedent 
# (left-hand-side) and the consequent (right-hand-side) is equal to the set {D,F,G}.
# Answer:
# {D} => {F,G} = {D,F,G}
# {F} => {D,G} = {D,F,G}
# {G} => {D,F} = {D,F,G}
# {D,F} => {G} = {D,F,G}
# {D,G} => {F} = {D,F,G}
# {F,G} => {D} = {D,F,G}
# {D,F,G} =>{} = {D,F,G}

# (2b)Organise the rules from (2a) into a lattice (please see the lecture slides about this). 
# No need to make a visualisation, just list the rules in each layer separately.
# Answer:

# #                  {D,F,G} =>{}
#         {F,G} => {D}    {D,G} => {F}   {D,F} => {G}
# {G} => {D,F}  {F} => {D,G}   {D} => {F,G}

# (2c) Calculate the support, confidence and lift of all the rules from (2a), 
# report by layers as in (2b).
# Answer:
# Support Calculation:
# {D,F,G} =>{}  Support = 40%   Confidence =  0.4/0.4 -> 100%   lift =  0.4/(.4*0) = 1
# {F,G} => {D}  Support = 40%   Confidence = .4/.533 -> 75%     lift =  0.4/(.533 * .7333) = 1.02
# {D,G} => {F}  Support = 40%   Confidence = .4/.4 -> 100%      lift =  0.4/(.4 *.8) = 1.25
# {D,F} => {G}  Support = 40%   Confidence = .4/.533 -> 75%     lift =  0.4/(.533 *.533) = 1.408
# {G} => {D,F}  Support = 40%   Confidence = .4/.533 -> 75%     lift =  0.4/(.533 *.533) = 1.408
# {F} => {D,G}  Support = 40%   Confidence = .4/.4 -> 100%      lift =  0.4/(.4 *.8) = 1.25
# {D} => {F,G}  Support = 40%   Confidence = .4/.533 -> 75%     lift =  0.4/(.533 * .533) = 1.408


# (2d) Find and report all rules from (2a) that have confidence at least 0.5 i.e 50%
# Answer:
# {D,F,G} =>{}    Confidence =  0.4/0.4 -> 100%   
# {F,G} => {D}    Confidence = .4/.533 -> 75%     
# {D,G} => {F}    Confidence = .4/.4 -> 100%     
# {D,F} => {G}    Confidence = .4/.533 -> 75%     
# {G} => {D,F}    Confidence = .4/.533 -> 75%     
# {F} => {D,G}    Confidence = .4/.4 -> 100%      
# {D} => {F,G}    Confidence = .4/.533 -> 75%   


# (2e) Apply the command rules = apriori(data,parameter=list(support=5/length(data),conf=0.5)). 
# See the results using inspect(rules) (do not print into report PDF) and
# report the rules with 3 items. 
# Note that the apriori command considers only 
# rules with 1 item in the consequent. Is this result in agreement with what you obtained in (2d)?

rules = apriori(data,parameter=list(support=5/length(data),conf=0.5))
inspect(rules)

# Answer: The results obtained by apriori command is in agreement with the result
# observed for question2d.

# Exercise 3

titanic = read.csv('titanic.csv')
head(titanic)
str(titanic)
table(titanic$Class)
table(titanic$Sex)
table(titanic$Age)
table(titanic$Survived)


library(arules)
rules1 = apriori(titanic)
inspect(rules1)

# (3a) How many rules did the apriori algorithm find? 
# (note that apriori was using the default parameters min support 0.1 and min confidence 0.8)
# Answer: 27 rules

# (3b) Consider all rules with confidence equal to 1.0. Which of these is the most interesting?
# One rule among them can explain all others, which one? (since this particular rule has 
# confidence 1.0, all other rules considered here have also confidence 1.0). 
# How would you explain these rules?
# Answer: 
# List of rules with confidence equal to 1.0 :
#   
# {Class=Crew}                       => {Age=Adult}
# {Class=Crew,Survived=No}           => {Age=Adult}
# {Class=Crew,Sex=Male}              => {Age=Adult} 
# {Class=Crew,Sex=Male,Survived=No}  => {Age=Adult}

# Explanation:
# ** Most interesting rule is {Class=Crew} => {Age=Adult} which has confidence of 1
#    Which means in all the occurences if an individual class is a Crew then it's age value is Adult.
#    In simple words, A Crew is always an adult in our dataset titanic.csv

# Explanation on why all other rules also have confidence 1.0
# Antecedent => Consequent 
       # X  -> Y ,  Now C(X->Y) = P(XUY)/P(X)                           
       #            Since confidence is 1.0 , we have P(XUY) = P(X)
       #            This explains all crew are adult and therefore all the antecedents containing
       #            Crew class will have no change in its support value when the consequent is adult.
# Therefore all the rules above have confidence as 1.
# All Antecedents in the rules are superset of {Crew} and Consequent is adult.i.e P(XUY) = P(X)


# (3c) Consider the two rules with the highest lift value (find them manually or use code from EX4
# to sort the rules by lift). These two rules have the same lift, the same support, 
# and the same confidence. Why? Hint: the reason is related to what you discovered in (3b).

# Answer:
# Two rules with highest lift value are given below:
# {Class=Crew,Survived=No}           => {Sex=Male}    0.3044071 0.9955423  1.2658514  670
# {Class=Crew,Age=Adult,Survived=No} => {Sex=Male}    0.3044071 0.9955423  1.2658514  670

# Since all Crew class are adults support for rules(superset of {Crew}) will be same with or without adult in the rules.
# Therefore support/support-count in these two rules are same.
# This also means support of Antecedents are equal and since both the rules have same Consequent,
# we have the equal confidence value.
# Lift (X->Y) = P(XUY)/(P(X)*P(Y)
# If we have two rules as (X1 -> Y) and (X2 -> Y)
# In above two rules P(X1) and P(X2) are equal and therefore P(X1 U Y) = P(X2 U Y) 
# Therefore two lifts are also equal.

# (3d) What is the most interesting rule in these results, 
# other than the ones discussed in (3b) and (3c)?
# Answer:
# {Sex=Male} => {Age=Adult} is the interesting rule because it has the highest support value
# and it also helps to understand that most frequent pattern consist of { Age=Adult, Sex=Males }.


# Exercise 4
# Consider the same Titanic dataset as in EX3. Please run the apriori algorithm again, but this 
# time with very low min support and min confidence, and sort by lift:

rules = apriori(titanic,parameter=list(supp=0.000001,conf=0.000001))
rules = as(rules,"data.frame")
rules = rules %>% arrange(-lift)

head(rules, n = 10) # remember you can show as many rows as you want by changing n

# (4a) Discuss what you can learn from the 3 rules with the highest lift.
# 
# rules                                                   support    confidence  lift    count
# 1    {Class=2nd,Sex=Male,Survived=Yes} => {Age=Child} 0.004997728  0.4400000 8.884771    11
# 2             {Class=2nd,Survived=Yes} => {Age=Child} 0.010904134  0.2033898 4.106982    24
# 3  {Class=2nd,Age=Adult,Survived=Yes} => {Sex=Female} 0.036347115  0.8510638 3.985514    80

# Answer: The chances of antecedents and consequents to occur together is far greater
#         even though the support of each of it is less which gives higher value of lift.
#         Lift is inversly proportional to support of antecedent and consequent and both
#         are positively correlated


# (4b) Calculate the support count of the antecedent (left-hand-side) in the rules of (4a)
# by dividing the count (last column) by confidence (3rd column).
# Which of these rules do you find the most interesting?
 
# 1. Support count = 25
# 2. Support count = 118
# 3. Support count = 94
# 
# Answer: {Class=2nd,Age=Adult,Survived=Yes} => {Sex=Female}is interesting because it has
# higher  confidence and greater lift value which indicates in the given pattern
# {Class=2nd,Age=Adult,Survived=Yes, Sex=Female} most female survived.


# (4c) Sort all rules by confidence.
# What can you learn from the 9 rules with confidence 1.0 and lift greater than 3?
rules_conf = rules %>% arrange(-confidence) %>% filter(lift > 3)
head(rules_conf, n = 9)

# Answer: More female child survived compared to male child.
#           Child survivors are more frequent even though the pattern in less i.e support therefor lift is greater


# (4d) Sort all rules by support. What can you learn from 
# the 4 rules with support greater than 0.7?
rules_supp = rules %>% arrange(-support) 
head(rules_supp, n = 4)

# Answer: Occurnce of Adult male is the most frequent patter in the data-sets






