---
title: "Homework10"
author: "Navedanjum Ansari"
date: "December 3, 2017"
output: pdf_document
---

## Excercise 1
```{r}
library(dplyr)
library(tidyr)
setwd("C:/Subjects/sem1/DataMining/submission/homework10")
library(dplyr)
require(data.table)
fread("instacart_1m.csv", sep = ",", header= TRUE)-> instacart1M

```

1.a   
```{r}
filter(instacart1M, order_id==2539329) %>% select(product_id, product_name) -> first_visit_orders
first_visit_orders

```

1.b How many rows remained ?      
```{r}
cleaned_data <- filter(instacart1M, product_id %in% first_visit_orders$product_id)
nrow(cleaned_data)


```
1.c   
```{r}
matrix_dataset <- dcast(cleaned_data, order_id~product_id, value.var='order_id')
head(matrix_dataset)

#Products present 
matrix_dataset[,2:6][!is.na(matrix_dataset[,2:6])] <- 1
#Product absence NA replaced by 0
matrix_dataset[is.na(matrix_dataset)] <- 0
#Dimensionality
dim(matrix_dataset)

```

1.d   
```{r}
#Eucledian distance

result_distances <- data.frame(Eucledian_distance = numeric())

for(i in 1:nrow(matrix_dataset)){
    result_distances <- sqrt((1-matrix_dataset[,2])^2 + 
                  (1-matrix_dataset[,3])^2 + (1-matrix_dataset[,4])^2 +
                  (1-matrix_dataset[,5])^2 + (1-matrix_dataset[,6])^2) }
   

#str(result_distances)
head(result_distances, n =250)


```

1.e     
```{r}
nearest_pt = 4
for(i in 1: 1825){
  if((result_distances[i] < nearest_pt) && (result_distances[i] != 0)){
     nearest_pt = result_distances[i]   } }

orderid_list <- numeric(0)
for(i in 1:1825){
  if(nearest_pt == result_distances[i] ){
     orderid_list = c(orderid_list, matrix_dataset[i,1]) } }

orderid_list
length(orderid_list)

```

1.f      
Answer: We should advertise Pistachios     

```{r}
filter(instacart1M, order_id %in% orderid_list,!(product_id %in% first_visit_orders$product_id)
       ) %>% select(order_id, product_id, product_name) -> recommendations
recommendations

(group_by(recommendations, product_name) %>% 
    summarise(Pcount = n()) %>% 
      arrange(desc(Pcount)))[1,1]   -> frequent_product

frequent_product

```

1.g      
Answer: Pistachios is found in the purchase of the customer A in the second visit orders

```{r}
#Finding other baskets for user_id: 1 user A
filter(instacart1M, user_id ==1) %>% distinct(order_id)

#order_id == 2398795

filter(instacart1M, order_id==2398795) %>% select(product_id, product_name) -> second_visit_orders
second_visit_orders

##Check if the closest orders arve available in this product list of second visit orders

second_visit_orders[second_visit_orders$product_name == 'Pistachios',]

```

## Excercise 2
```{r}
setwd("C:/Subjects/sem1/DataMining/submission/homework10")
library(dplyr)
require(data.table)
fread("instacart_1m.csv", sep = ",", header= TRUE)-> instacart1M
head(instacart1M)

```

2a. What is the overall most popular product?    
Answer: Banana is the most popular product     

```{r}
grouped_data = group_by(instacart1M, product_name)
summarise(grouped_data, total_count = n()) %>% top_n(1, total_count)
```

2b. Popular product at 23, 00 ... 01 hour?
Answer: Most popular products for 00...23 are Banana or Bag of Organic Bananas   
           
        

```{r}
grouped_data = group_by(instacart1M, order_hour_of_day, product_name)
summarise(grouped_data, total_count = n()) %>% top_n(1, total_count) %>% 
                                        subset( !duplicated(order_hour_of_day)) -> result_set


result_set[,1:2]

```

2c. Find the relative popularity of the product   
2d. 
```{r}
grouped_data1 = group_by(instacart1M, product_name)
summarise(grouped_data, total_count = n()) -> total_product_count

grouped_data2 = group_by(instacart1M, order_hour_of_day, product_name)
summarise(grouped_data2, total_count = n()) -> hourly_product_count

hourly_pdata <-dcast(hourly_product_count,product_name~order_hour_of_day,value.var='total_count')
prod_name <- hourly_pdata$product_name

#Seting to null for further calculation
hourly_pdata$product_name<-NULL
hourly_pdata[is.na(hourly_pdata)]<-0

sdata <- apply(hourly_pdata,1,sum)
relative_data  <- hourly_pdata/sdata

#Reattaching the product_name
relative_data$product_name<- prod_name


colMax <- function(data) sapply(data, max, na.rm = TRUE)

max_relative_sales <- colMax(relative_data[,1:24])

#As relative frequency is 1 i.e sale_for_that_hour is same as sales_total
#Finding product names

product_list <- c()

df <- data.frame(relative_data)

#Iterated manually 24 times to get product list
for( j in 1:24) {
  for ( i in 1:31306) {
     if(df[i,j] == 1){
      product_list  <- c(product_list, df[i,25] )
      break
    }
} }

product_list
  
```

Answer: Maximum relative frequency is 1 therefore the products are purchased only for that hours such 
        such that total sales and hourly sales are equal   

```{r}


```

## Excercise 3    

Comic characters data     
Dataset link: https://github.com/fivethirtyeight/data/tree/master/comic-characters     

Following are the datamining goals:      
Goal 1: What type of new comic character should be introduced in market  ?  
Goal 2: To find type of comic characters gaining/decreasing popularity  in recent decades    
Goal 3: Does gender of a comic character play an important role in it's popularity    
Goal 4: Gender of readers and authors to find out the which gender     
        dominate the comic market     
Goal 5: Preferred appearance of comic characters 
Goal 6: Preferred character of comic weather Good or Evil       


Subtask to achieve the goals:     
1. Data cleaning to replace and remove incorrect or missing data    
2. Data loading in R or any other analytics tool   
3. Performing aggregation on data variables     
4. Finding correlation of the variables     
5. Plotting graphs for visualization     
6. Conclusion from the observation and facts produced     


Datamining techniques to achieve goals:       
1. Association techniques to determine most preferred comic characters    
   based on apperance and charcter(Good vs Evil).    
2. Classification techniques to find categorize the existing character 
   based on popularity and year in which it was popular to draw prediction   
   for introducing new character and probability of it working well in comic market   
3. Visualization plots and charts to understand the past and current trends.  
   to predict the future trend such as femal comic character , male comic charcter    
   popularity since first appearance etc.
4. Aggregation and Correlations.    
   
   
   











